{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "np.set_printoptions(linewidth=1000, edgeitems=30)\n",
    "np.random.seed(20160501)  # setting seed for reproducibility\n",
    "\n",
    "plot_dir = '../plots/'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Illustrative proof of concept\n",
    "\n",
    "Given some function, we want to create a sample of points in such a way that they are likely to be around the maximum of that function, and unlikely to be at the minimum of that function.\n",
    "\n",
    "To illustrate, let's look at a simple function $f(x) = 25 - x^2$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    return 25 - x**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.linspace(start=-6, stop=6, endpoint=True, num=251)\n",
    "y = f(x)\n",
    "plt.plot(x,y)\n",
    "plt.ylim([0, 25])\n",
    "plt.xlim([-6, 6])\n",
    "plt.tight_layout()\n",
    "plt.savefig(f'{plot_dir}example.pdf')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To create our function-specific sample, we first take a sample uniformly at random along the x-axis. These points are potential candidates for our final sample."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sample = np.random.uniform(low=-5, high=5, size=50)\n",
    "\n",
    "plt.plot(x,y)\n",
    "plt.scatter(sample, f(sample))\n",
    "plt.ylim([0, 25])\n",
    "plt.xlim([-6, 6])\n",
    "plt.tight_layout()\n",
    "plt.savefig(f'{plot_dir}example-raw.pdf')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So how do we decide whether or not to keep each of these points?\n",
    "\n",
    "We draw another uniform sample, one for each point from our previous sample. These values are scaled to the y-range we are looking at, so [0, 25] in this case.\n",
    "Then we compare the actual value of our sample to its matched value from this second sample. We accept each point that is lower than its matched value, and ignore each value that is higher than its matched value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "matched_values = np.random.uniform(low=0, high=25, size=50)\n",
    "\n",
    "plt.plot(x,y)\n",
    "plt.scatter(sample, f(sample))\n",
    "plt.scatter(sample, matched_values, color='C1')\n",
    "plt.vlines(sample, f(sample), matched_values, alpha=.5)\n",
    "plt.fill_between(x, y, color='green', alpha=.3)\n",
    "plt.fill_between(x, 25, y, color='red', alpha=.3)\n",
    "plt.ylim([0, 25])\n",
    "plt.xlim([-6, 6])\n",
    "plt.tight_layout()\n",
    "plt.savefig(f'{plot_dir}example-matched.pdf')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "accept = matched_values < f(sample)\n",
    "ignore = matched_values >= f(sample)\n",
    "\n",
    "plt.plot(x,y, zorder=1)\n",
    "plt.fill_between(x, y, color='green', alpha=.3)\n",
    "plt.fill_between(x, 25, y, color='red', alpha=.3)\n",
    "plt.scatter(sample[accept], matched_values[accept], color='green', zorder=2)\n",
    "plt.scatter(sample[accept], f(sample[accept]), color='C0', zorder=2)\n",
    "plt.vlines(sample[accept], f(sample[accept]), matched_values[accept], alpha=.5)\n",
    "plt.scatter(sample[ignore], matched_values[ignore], color='red', alpha=.5, zorder=3)\n",
    "# plt.scatter(sample[ignore], f(sample[ignore]), color='red', alpha=.5, zorder=3)\n",
    "plt.vlines(sample[ignore], f(sample[ignore]), matched_values[ignore], alpha=.5)\n",
    "plt.ylim([0, 25])\n",
    "plt.xlim([-6, 6])\n",
    "plt.tight_layout()\n",
    "plt.savefig(f'{plot_dir}example-included-excluded.pdf')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# For 2d functions\n",
    "## Some initial setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def row_vectorize(func):\n",
    "    def new_func(X):\n",
    "        try:\n",
    "            return np.array([func(row) for row in X])\n",
    "        except TypeError:\n",
    "            return func(X)\n",
    "    return new_func\n",
    "\n",
    "\n",
    "@row_vectorize\n",
    "def hm(xx):\n",
    "    \"\"\"\n",
    "    HIMMELBLAU FUNCTION\n",
    "\n",
    "    INPUT:\n",
    "    xx = [x1, x2]\n",
    "    \"\"\"\n",
    "    x1, x2 = xx\n",
    "\n",
    "    term1 = (x1**2 + x2 - 11)**2\n",
    "    term2 = (x2**2 + x1 - 7)**2\n",
    "\n",
    "    return term1 + term2\n",
    "\n",
    "\n",
    "@row_vectorize\n",
    "def sphere(xx):\n",
    "    return np.sum([x**2 for x in xx])\n",
    "\n",
    "hm.name = 'hm'\n",
    "hm.min_val = 0\n",
    "hm.max_val = 450\n",
    "\n",
    "sphere.name = 'sphere'\n",
    "sphere.min_val = 0\n",
    "sphere.max_val = 50\n",
    "\n",
    "\n",
    "fit_func = sphere"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Determining the high/low range of values for this function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "upper_lim, lower_lim = 5, -5\n",
    "step_size = 0.1\n",
    "\n",
    "num_steps = int((upper_lim-lower_lim)//step_size + 4)\n",
    "linespec = np.linspace(lower_lim-step_size, upper_lim+step_size, num_steps)\n",
    "\n",
    "X, Y = np.meshgrid(linespec, linespec)\n",
    "Z = np.array([fit_func(x_y) for x_y in zip(X.flatten(), Y.flatten())])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.pcolor(X, Y, Z.reshape((num_steps, num_steps)), cmap='viridis_r')\n",
    "plt.colorbar()\n",
    "plt.tight_layout()\n",
    "plt.savefig(f'plots/{fit_func.name}.pdf')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "percentage = .95\n",
    "\n",
    "min_val, max_val = Z.min(), Z.max()\n",
    "print(f'min: {min_val}, max: {max_val}')\n",
    "\n",
    "print(f'{int(np.round(percentage*100))}th percentile: {np.sort(-Z)[int(Z.size*percentage)]}')\n",
    "\n",
    "plt.hist(Z, cumulative=True, density=True, bins=100)\n",
    "plt.axhline(y=percentage, color='black', alpha=.5)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Although the lowest value is -900, only 5% of all values are below ~450, so a slightly higher 'minimum value' should be used when determinning the sampling range."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Function-dependent sampling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_samples = 1000\n",
    "ndim = 2\n",
    "oversampling_factor = 1.25\n",
    "min_probability = 0.0\n",
    "min_val, max_val = fit_func.min_val, fit_func.max_val\n",
    "\n",
    "raw_sample = np.random.uniform(high=upper_lim, low=lower_lim, size=(int(n_samples*oversampling_factor), ndim))\n",
    "f_values = fit_func(raw_sample)\n",
    "f_probabilities = (f_values - min_val)/(max_val - min_val)\n",
    "f_probabilities = (1-min_probability) * f_probabilities + min_probability\n",
    "\n",
    "check_values = np.random.uniform(size=f_probabilities.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A quick comparison of the uniform `check_values` distribution (orange) and the probability of accepting/keeping a uniformly sampled point in the search space according to the above calculations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(f_probabilities, label='function')\n",
    "plt.hist(check_values, alpha=0.5, label='uniform')\n",
    "plt.legend(loc=0)\n",
    "\n",
    "print(f\"{sum(f_probabilities > check_values)}/{int(n_samples*oversampling_factor)}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scatter_size = 12\n",
    "\n",
    "plt.pcolor(X, Y, Z.reshape((num_steps, num_steps)), cmap='viridis_r')\n",
    "plt.colorbar()\n",
    "plt.scatter(raw_sample[:,0], raw_sample[:,1], color='orange', s=scatter_size, label='uniform sample')\n",
    "plt.legend(loc=4)\n",
    "plt.tight_layout()\n",
    "plt.savefig(f'{plot_dir}{fit_func.name}-raw.pdf')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_samples = raw_sample[f_probabilities < check_values]\n",
    "non_samples = raw_sample[f_probabilities >= check_values]\n",
    "\n",
    "scatter_size = 12\n",
    "\n",
    "plt.pcolor(X, Y, Z.reshape((num_steps, num_steps)), cmap='viridis_r')\n",
    "plt.colorbar()\n",
    "plt.scatter(new_samples[:,0], new_samples[:,1], color='orange', s=scatter_size, label='included in sample')\n",
    "plt.scatter(non_samples[:,0], non_samples[:,1], color='red', s=scatter_size, label='excluded from sample')\n",
    "plt.legend(loc=4)\n",
    "plt.tight_layout()\n",
    "plt.savefig(f'{plot_dir}{fit_func.name}-included-excluded.pdf')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Now as a function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sample_by_function(func, ndim, num_samples, min_val, max_val, upper_bound, lower_bound, *, target='minimization', min_probability=0.0):\n",
    "\n",
    "    target = target.lower()\n",
    "    if target not in ['minimization', 'maximization']:\n",
    "        raise ValueError(f\"Invalid optimization target '{target}', please choose 'minimization' or 'maximization' instead.\")\n",
    "    \n",
    "    oversampling_factor = 2.5\n",
    "    new_sample = np.array([]).reshape((0, ndim))\n",
    "    \n",
    "    while len(new_sample) < num_samples:\n",
    "        raw_sample = np.random.uniform(high=upper_bound, low=lower_bound, size=(int(num_samples*oversampling_factor), ndim))\n",
    "        f_values = func(raw_sample)\n",
    "        f_probabilities = (f_values - min_val)/(max_val - min_val)\n",
    "        f_probabilities = (1-min_probability) * f_probabilities + min_probability\n",
    "\n",
    "        check_values = np.random.uniform(size=f_probabilities.shape)\n",
    "        if target == 'minimization':\n",
    "            sample_filter = f_probabilities < check_values\n",
    "        elif target == 'maximization':\n",
    "            sample_filter = f_probabilities > check_values\n",
    "        \n",
    "        new_sample = np.vstack((new_sample, raw_sample[sample_filter]))\n",
    "    \n",
    "    return new_sample[:num_samples]        "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "samples = sample_by_function(fit_func, 2, 100, fit_func.min_val, fit_func.max_val, 5, -5)\n",
    "\n",
    "plt.pcolor(X, Y, Z.reshape((num_steps, num_steps)), cmap='viridis_r')\n",
    "plt.colorbar()\n",
    "plt.scatter(samples[:,0], samples[:,1], s=scatter_size, label='sample', color='orange')\n",
    "plt.legend(loc=4)\n",
    "plt.tight_layout()\n",
    "plt.savefig(f'{plot_dir}{fit_func.name}-simple-sample.pdf')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
